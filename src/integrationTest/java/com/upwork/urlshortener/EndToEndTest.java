package com.upwork.urlshortener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.upwork.urlshortener.configuration.AppProperties;
import com.upwork.urlshortener.controller.request.ShortenRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@SpringBootTest
@WebAppConfiguration
public class EndToEndTest {

    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private AppProperties appProperties;

    @Autowired
    private ObjectMapper objectMapper;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void givenHomePageURI_whenMockMVC_thenReturnsIndexJSPViewName() throws Exception {
        String payload = objectMapper.writeValueAsString(ShortenRequest.builder().url("https://google.com").build());
        this.mockMvc.perform(post("/").contentType(MediaType.APPLICATION_JSON).header(AUTHORIZATION,
                appProperties.getApiKey()).content(payload)).andExpect(status().isOk());
    }

}
