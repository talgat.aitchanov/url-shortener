package com.upwork.urlshortener.filter;

import com.upwork.urlshortener.configuration.AppProperties;
import org.apache.catalina.connector.Response;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {AuthFilter.class})
@ExtendWith(SpringExtension.class)
class AuthFilterTest {
    @Autowired
    private AuthFilter authFilter;

    @MockBean
    private AppProperties appProperties;

    @Test
    void doFilter_success() throws IOException, ServletException {
        HttpServletRequestWrapper httpServletRequestWrapper = new HttpServletRequestWrapper(new HttpServletRequestWrapper(new HttpServletRequestWrapper(new HttpServletRequestWrapper(new MockHttpServletRequest()))));
        Response response = new Response();
        FilterChain filterChain = mock(FilterChain.class);
        doNothing().when(filterChain).doFilter(any(), any());
        authFilter.doFilter(httpServletRequestWrapper, response, filterChain);
        verify(filterChain).doFilter(any(), any());
        assertFalse(httpServletRequestWrapper.isTrailerFieldsReady());
    }

    @Test
    void doFilterThrowsUnauthorizedException() throws ServletException, IOException {
        MockHttpServletRequest mockHttpServletRequest = new MockHttpServletRequest();
        mockHttpServletRequest.setMethod(HttpMethod.POST.name());
        mockHttpServletRequest.setServletPath("/");
        MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();
        HttpServletRequestWrapper request = new HttpServletRequestWrapper(mockHttpServletRequest);
        HttpServletResponseWrapper response = new HttpServletResponseWrapper(mockHttpServletResponse);
        FilterChain filterChain = mock(FilterChain.class);
        when(appProperties.getApiKey()).thenReturn("");
        authFilter.doFilter(request, response, filterChain);
        verifyNoInteractions(filterChain);
    }
}

