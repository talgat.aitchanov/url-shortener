package com.upwork.urlshortener.service;

import com.upwork.urlshortener.configuration.AppProperties;
import com.upwork.urlshortener.exception.InvalidURLException;
import com.upwork.urlshortener.repository.UrlRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {ShortenService.class, AppProperties.class})
@ExtendWith(SpringExtension.class)
class ShortenServiceTest {
    @Autowired
    private AppProperties appProperties;

    @MockBean
    private HashingService hashingService;

    @Autowired
    private ShortenService shortenService;

    @MockBean
    private UrlRepository urlRepository;

    @Test
    void encode_success() {
        when(urlRepository.save(any(), any())).thenReturn("https://example.org/example");
        when(hashingService.hash(any())).thenReturn("https://example.org/example");
        assertEquals("nullhttps://example.org/example", shortenService.encode("https://example.org/example"));
        verify(urlRepository).save(any(), any());
        verify(hashingService).hash(any());
    }

    @Test
    void encode_throwsInvalidURLException() {
        when(urlRepository.save(any(), any())).thenReturn("https://example.org/example");
        when(hashingService.hash(any())).thenReturn("https://example.org/example");
        assertThrows(InvalidURLException.class, () -> shortenService.encode("UU"));
    }

    @Test
    void findUrl_success() {
        when(urlRepository.find(any())).thenReturn("https://example.org/example");
        assertEquals("https://example.org/example", shortenService.findUrl("https://example.org/example"));
        verify(urlRepository).find(any());
    }

}

