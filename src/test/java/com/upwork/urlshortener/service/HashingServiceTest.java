package com.upwork.urlshortener.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {HashingService.class})
@ExtendWith(SpringExtension.class)
class HashingServiceTest {
    @Autowired
    private HashingService hashingService;

    @Test
    void hash_success() {
        assertEquals("cc70df7e", hashingService.hash("https://example.org/example"));
    }
}

