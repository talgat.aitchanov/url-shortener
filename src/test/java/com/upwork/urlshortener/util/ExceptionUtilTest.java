package com.upwork.urlshortener.util;

import org.apache.logging.log4j.util.Strings;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExceptionUtilTest {

    @Test
    void returnEmptyString_success() {
        assertEquals(Strings.EMPTY, ExceptionUtil.collectMessages(new Throwable()));
    }

    @Test
    void collectMessages_success() {
        String test_message = "test message";
        assertEquals(test_message, ExceptionUtil.collectMessages(new Throwable(test_message)));
    }
}

