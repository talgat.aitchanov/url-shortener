package com.upwork.urlshortener.util;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class UrlValidatorUtilTest {


    @Test
    void testIsValid() {
        assertTrue(UrlValidatorUtil.isValid("https://example.org/example"));
        assertFalse(UrlValidatorUtil.isValid("foo"));
        assertFalse(UrlValidatorUtil.isValid("[::FFFF:999.999.999.999]:9U"));
        assertFalse(UrlValidatorUtil.isValid("https://example.org/examplehttps://example.org/example"));
        assertFalse(UrlValidatorUtil.isValid("Valuehttps://example.org/example"));
    }
}

