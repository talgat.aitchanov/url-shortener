package com.upwork.urlshortener.repository;

import com.upwork.urlshortener.configuration.RedisConfiguration;
import com.upwork.urlshortener.exception.NotFoundException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {UrlRepository.class, RedisConfiguration.class})
@ExtendWith(SpringExtension.class)
class UrlRepositoryTest {
    @Autowired
    private RedisConfiguration redisConfiguration;

    @MockBean(answer = Answers.RETURNS_DEEP_STUBS)
    private RedisTemplate redisTemplate;

    @Autowired
    private UrlRepository urlRepository;

    @AfterEach
    void cleanUp() {
        Mockito.reset(redisTemplate);
    }

    @Test
    void save_throwsNotFoundException() {
        when(redisTemplate.opsForValue()).thenThrow(new NotFoundException("someId"));
        assertThrows(NotFoundException.class, () -> urlRepository.save("someId", "https://example.org/example"));
        verify(redisTemplate).opsForValue();
    }

    @Test
    void find_success() {
        when(redisTemplate.opsForValue().get(anyString())).thenReturn("https://example.org/example");
        String expandedUrl = urlRepository.find("someId");
        assertEquals("https://example.org/example", expandedUrl);
    }

    @Test
    void find_throwsNotFoundException() {
        when(redisTemplate.opsForValue().get(anyString())).thenReturn(null);
        assertThrows(NotFoundException.class, () -> urlRepository.find("someId"));
    }
}

