package com.upwork.urlshortener.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.upwork.urlshortener.configuration.AppProperties;
import com.upwork.urlshortener.controller.request.ShortenRequest;
import com.upwork.urlshortener.service.ShortenService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@TestPropertySource("classpath:application-test.properties")
@EnableConfigurationProperties(value = AppProperties.class)
@ContextConfiguration(classes = {Controller.class})
@ExtendWith(SpringExtension.class)
class ControllerTest {
    private static final ObjectMapper mapper = new ObjectMapper();
    @Autowired
    private Controller controller;
    @Autowired
    private AppProperties appProperties;
    @MockBean
    private ShortenService shortenService;

    @Test
    void getById() throws Exception {
        when(shortenService.findUrl(any())).thenReturn("http://localhost:8080/test");
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/{id}", "42");
        getResult.characterEncoding("Encoding");
        MockMvcBuilders.standaloneSetup(controller).build().perform(getResult).andExpect(MockMvcResultMatchers.status().isFound()).andExpect(MockMvcResultMatchers.redirectedUrl("http://localhost:8080/test"));
    }

    @Test
    void postAndGenerateId() throws Exception {
        String payload = mapper.writeValueAsString(ShortenRequest.builder().url("https://google.com").build());
        when(shortenService.encode(any())).thenReturn("http://localhost:8080/test");
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/").header(AUTHORIZATION, appProperties.getApiKey()).contentType(MediaType.APPLICATION_JSON).content(payload);
        MockMvcBuilders.standaloneSetup(controller).build().perform(requestBuilder).andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=ISO-8859-1")).andExpect(MockMvcResultMatchers.content().string("http://localhost:8080/test"));
    }
}

