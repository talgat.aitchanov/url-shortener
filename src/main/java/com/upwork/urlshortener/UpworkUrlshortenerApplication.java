package com.upwork.urlshortener;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
    public class UpworkUrlshortenerApplication {

    public static void main(String[] args) {
        SpringApplication.run(UpworkUrlshortenerApplication.class, args);
    }

}
