package com.upwork.urlshortener.service;

import com.google.common.hash.Hashing;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;

@Service
public class HashingService {

    public String hash(String url) {
        return Hashing.murmur3_32_fixed().hashString(url, StandardCharsets.UTF_8).toString();
    }

}
