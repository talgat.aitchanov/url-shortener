package com.upwork.urlshortener.service;

import com.upwork.urlshortener.configuration.AppProperties;
import com.upwork.urlshortener.exception.InvalidURLException;
import com.upwork.urlshortener.repository.UrlRepository;
import com.upwork.urlshortener.util.UrlValidatorUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Slf4j
@Service
public class ShortenService {

    private final UrlRepository urlRepository;
    private final HashingService hashingService;
    private final AppProperties appProperties;

    public String encode(String url) {
        if (UrlValidatorUtil.isValid(url)) {
            final String id = hashingService.hash(url);
            return appProperties.getShortenUrl() + urlRepository.save(id, url);
        }
        String errorMessage = String.format("The value you provided \"%s\" is not URL format", url);
        throw new InvalidURLException(errorMessage);
    }

    public String findUrl(String id) {
        return urlRepository.find(id);
    }

}
