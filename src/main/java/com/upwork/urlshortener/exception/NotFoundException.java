package com.upwork.urlshortener.exception;

import com.upwork.urlshortener.exception.model.ErrorCode;
import lombok.Getter;

public class NotFoundException extends RuntimeException {

    @Getter
    private final ErrorCode errorCode;

    public NotFoundException(String message) {
        super(message);
        this.errorCode = ErrorCode.URL_NOT_FOUND;
    }
}
