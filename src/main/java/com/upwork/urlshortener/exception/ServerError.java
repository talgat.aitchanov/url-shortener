package com.upwork.urlshortener.exception;

import com.upwork.urlshortener.exception.model.ErrorCode;
import lombok.Getter;

public class ServerError extends RuntimeException {

    @Getter
    private final ErrorCode errorCode;

    public ServerError(String message) {
        super(message);
        this.errorCode = ErrorCode.INTERNAL_ERROR;
    }
}
