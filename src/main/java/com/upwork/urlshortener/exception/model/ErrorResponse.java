package com.upwork.urlshortener.exception.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponse {
    String errorCode;
    String errorMessage;
    String errors;

    public static ErrorResponse of(String errorCode, String errorMessage, String errors) {
        return ErrorResponse.builder()
                .errorCode(errorCode)
                .errorMessage(errorMessage)
                .errors(errors)
                .build();
    }
}
