package com.upwork.urlshortener.exception.model;

public enum ErrorCode {
    INVALID_URL,
    URL_NOT_FOUND,
    UNAUTHORIZED,
    INTERNAL_ERROR
}
