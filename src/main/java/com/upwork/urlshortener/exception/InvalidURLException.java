package com.upwork.urlshortener.exception;

import com.upwork.urlshortener.exception.model.ErrorCode;
import lombok.Getter;

public class InvalidURLException extends RuntimeException{

    @Getter
    private final ErrorCode errorCode;

    public InvalidURLException(String message) {
        super(message);
        this.errorCode = ErrorCode.INVALID_URL;
    }
}
