package com.upwork.urlshortener.configuration;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.testcontainers.containers.DockerComposeContainer;

import javax.annotation.PreDestroy;
import java.io.File;

@Component
@Profile("test")
public class Docker {

    public static final DockerComposeContainer environment = new DockerComposeContainer(new File("docker-compose-test.yml"))
            .withExposedService("test_cache_1", 6379);

    static {
        environment.start();
    }

    @PreDestroy
    public void destroy() {
        environment.stop();
    }

}
