package com.upwork.urlshortener.util;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@UtilityClass
public class ExceptionUtil {

    public static String collectMessages(Throwable t) {
        if (StringUtils.isBlank(t.getMessage())) {
            return Strings.EMPTY;
        }
        List<String> messages = new LinkedList<>();
        while (Objects.nonNull(t)) {
            messages.add(t.getMessage());
            t = t.getCause();
        }
        return String.join(", ", messages);
    }

}
