package com.upwork.urlshortener.util;

import lombok.experimental.UtilityClass;
import org.apache.commons.validator.routines.UrlValidator;

@UtilityClass
public class UrlValidatorUtil {

    private static final UrlValidator urlValidator = new UrlValidator(new String[]{"http", "https"});

    public static boolean isValid(String value) {
        return urlValidator.isValid(value);
    }

}
