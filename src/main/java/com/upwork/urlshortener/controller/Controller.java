package com.upwork.urlshortener.controller;

import com.upwork.urlshortener.controller.request.ShortenRequest;
import com.upwork.urlshortener.service.ShortenService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@AllArgsConstructor
@RestController
public class Controller {

    private ShortenService shortenService;

    @PostMapping("/")
    public ResponseEntity<String> shortenUrl(@RequestBody ShortenRequest shortenRequest) {
        return ResponseEntity.ok(shortenService.encode(shortenRequest.getUrl()));
    }

    @GetMapping("/{id}")
    public void redirect(@PathVariable("id") String id, HttpServletResponse response) throws IOException {
        response.sendRedirect(shortenService.findUrl(id));
    }

}
