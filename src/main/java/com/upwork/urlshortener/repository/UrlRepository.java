package com.upwork.urlshortener.repository;

import com.upwork.urlshortener.configuration.RedisConfiguration;
import com.upwork.urlshortener.exception.NotFoundException;
import com.upwork.urlshortener.exception.ServerError;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Slf4j
@AllArgsConstructor
@Repository
public class UrlRepository {

    private final RedisTemplate<String, String> redisTemplate;
    private final RedisConfiguration redisConfiguration;

    public String save(String id, String url) {
        try {
            redisTemplate.opsForValue().set(id, url, redisConfiguration.getTimeToLiveSec(), TimeUnit.SECONDS);
            return id;
        } catch (RedisConnectionFailureException e) {
            log.error(e.getMessage(), e);
            throw new ServerError(e.getMessage());
        }
    }

    public String find(String id) {
        try {
            return Optional.ofNullable(redisTemplate.opsForValue().get(id)).orElseThrow(() -> {
                String errorMessage = String.format("id %s is not found", id);
                log.error(errorMessage);
                throw new NotFoundException(errorMessage);
            });
        } catch (RedisConnectionFailureException e) {
            log.error(e.getMessage(), e);
            throw new ServerError(e.getMessage());
        }
    }

}
