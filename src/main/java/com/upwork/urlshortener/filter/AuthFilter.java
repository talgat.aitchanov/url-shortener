package com.upwork.urlshortener.filter;

import com.upwork.urlshortener.configuration.AppProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RequiredArgsConstructor
@Slf4j
@Component
public class AuthFilter extends HttpFilter {
    private static final List<String> PROTECTED_PATHS = List.of("/");
    private final AppProperties appProperties;

    @Override
    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (HttpMethod.POST.name().equals(request.getMethod()) && PROTECTED_PATHS.contains(request.getServletPath())) {
            boolean isAuthorized = checkAuthorizationHeader(request);
            if (!isAuthorized) {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }
        }
        chain.doFilter(request, response);
    }

    private boolean checkAuthorizationHeader(HttpServletRequest request) {
        String authHeader = request.getHeader(AUTHORIZATION);
        return StringUtils.equals(appProperties.getApiKey(), authHeader);
    }
}