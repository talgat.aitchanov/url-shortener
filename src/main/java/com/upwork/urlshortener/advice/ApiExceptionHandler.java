package com.upwork.urlshortener.advice;

import com.upwork.urlshortener.exception.InvalidURLException;
import com.upwork.urlshortener.exception.NotFoundException;
import com.upwork.urlshortener.exception.ServerError;
import com.upwork.urlshortener.exception.model.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import static com.upwork.urlshortener.util.ExceptionUtil.collectMessages;

@Slf4j
@RestControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected @NotNull ResponseEntity<Object> handleHttpMessageNotReadable(@NotNull HttpMessageNotReadableException ex, @NotNull HttpHeaders headers, @NotNull HttpStatus status, @NotNull WebRequest request) {
        log.error(ex.getMessage(), ex);
        var body = buildErrorResponse(HttpStatus.BAD_REQUEST.name(), ex.getMessage(), collectMessages(ex));
        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    @Override
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    protected ResponseEntity<Object> handleMethodArgumentNotValid(@NotNull MethodArgumentNotValidException ex, @NotNull HttpHeaders headers, @NotNull HttpStatus status, @NotNull WebRequest request) {
        log.error(ex.getMessage(), ex);

        List<String> errors = new ArrayList<>();
        for (FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
            errors.add(fieldError.getField() + ": " + fieldError.getDefaultMessage());
        }
        var body = buildErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY.name(), "Validation error. Check 'errors' field" +
                " for details.", String.join("; ", errors));
        return ResponseEntity.unprocessableEntity().body(body);
    }

    @ExceptionHandler(value = NotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ErrorResponse notFoundException(NotFoundException ex) {
        log.error(ex.getMessage(), ex);
        return buildErrorResponse(HttpStatus.NOT_FOUND.name(), ex.getMessage());
    }

    @ExceptionHandler(value = InvalidURLException.class)
    @ResponseStatus(value = HttpStatus.BAD_GATEWAY)
    public ErrorResponse invalidURLException(InvalidURLException ex) {
        log.error(ex.getMessage(), ex);
        return buildErrorResponse(HttpStatus.BAD_GATEWAY.name(), ex.getMessage(), "Provided URL is incorrect");
    }

    @ExceptionHandler(value = ClassNotFoundException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse classNotFoundException(ClassNotFoundException ex) {
        log.error(ex.getMessage(), ex);
        return buildErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.name(), ex.getMessage(), "Class Not " +
                "Found On The Classpath");
    }

    @ExceptionHandler(value = InvocationTargetException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse invocationTargetException(InvocationTargetException ex) {
        log.error(ex.getMessage(), ex);
        return buildErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.name(), ex.getMessage(), "Failed To " +
                "Invoke Method or Constructor");
    }

    @ExceptionHandler(value = ServerError.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse exception(ServerError ex) {
        log.error(ex.getMessage(), ex);
        return buildErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.name(), ex.getMessage());
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse exception(Exception ex) {
        log.error(ex.getMessage(), ex);
        return buildErrorResponse(HttpStatus.BAD_REQUEST.name(), ex.getMessage());
    }

    private ErrorResponse buildErrorResponse(String errorCode, String errorMessage) {
        return buildErrorResponse(errorCode, errorMessage, "");
    }

    private ErrorResponse buildErrorResponse(String errorCode, String errorMessage, String errors) {
        return ErrorResponse.of(errorCode, errorMessage, errors);
    }

}
